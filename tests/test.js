const assert = require('assert');
const sum = require('../src/index');

describe('Suma', function() {
  it('debería retornar 3 para 1 + 2', function() {
    assert.strictEqual(sum(1, 2), 3);
  });

  it('debería retornar 5 para 2 + 3', function() {
    assert.strictEqual(sum(2, 3), 5);
  });
});
